# Forgot my handcuffs

Game made for the Lundum Dare 46

[![Version](http://img.shields.io/badge/Version-0.0.1-blue.svg)]()
[![Version](http://img.shields.io/badge/Progession-3%-orange.svg)]()
[![Version](http://img.shields.io/badge/Available-NO-red.svg)]()

## Authors:
* https://ldjam.com/users/index/
* https://ldjam.com/users/lightin/

## Tools:
- Godot Engine 3.2.1
- Paint / Krita / Gimp
- Git

## ScreenShot:
### Main Menu
![Main Menu](./readme/screenshot_menu.png?raw=true "Main Menu")
### Game View
![Game View](./readme/screenshot_game.png?raw=true "Game View")
### Options Menu
![Options Menu](./readme/screenshot_options.png?raw=true "Options Menu")

## Lisence:
Creative Commons Zero
