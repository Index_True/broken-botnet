extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var audio = get_tree().get_root().get_node("Audio").get_node("Effect")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass



func _on_Sound_Slider_Master_value_changed(value):
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Master"), value)
	audio.play()



func _on_Sound_Slider_Music_value_changed(value):
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Music"), value)
	audio.play()


func _on_Sound_Slider_SoundEffect_value_changed(value):
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("SoundEffect"), value)
	audio.play()




func _on_Music_Mute_pressed():
	AudioServer.set_bus_mute(AudioServer.get_bus_index("Music"), !AudioServer.is_bus_mute(AudioServer.get_bus_index("Music")))
	audio.play()

func _on_SoundEffect_Mute_pressed():
	AudioServer.set_bus_mute(AudioServer.get_bus_index("SoundEffect"),  !AudioServer.is_bus_mute(AudioServer.get_bus_index("SoundEffect")))
	audio.play()





















func _on_BackToMenu_pressed():
	audio.play()
	get_tree().change_scene("res://scenes/StartMenu/StartMenu.tscn")








