extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var audio = get_tree().get_root().get_node("Audio").get_node("Effect")


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.



# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Play_pressed():
	audio.play()
	get_tree().change_scene("res://ui/HUD/HUD.tscn")



func _on_Options_pressed():
	audio.play()
	get_tree().change_scene("res://scenes/Options/Options.tscn")



func _on_Credits_pressed():
	audio.play()


func _on_Quit_pressed():
	audio.play()
	get_tree().quit()

