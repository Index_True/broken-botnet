extends Container


onready var audio = get_tree().get_root().get_node("Audio").get_node("Effect")


func _input(event):
	if event.is_action_pressed("ui_cancel"):
		get_tree().paused = not get_tree().paused
		self.visible = not self.visible
		AudioServer.set_bus_effect_enabled(AudioServer.get_bus_index("Music"),1,get_tree().paused)



func _on_Resume_pressed():
	audio.play()
	get_tree().paused = not get_tree().paused
	self.visible = not self.visible
	AudioServer.set_bus_effect_enabled(AudioServer.get_bus_index("Music"),1,get_tree().paused)




func _on_MainMenu_pressed():
	audio.play()
	get_tree().paused = not get_tree().paused
	AudioServer.set_bus_effect_enabled(AudioServer.get_bus_index("Music"),1,get_tree().paused)
	get_tree().change_scene("res://scenes/StartMenu/StartMenu.tscn")
