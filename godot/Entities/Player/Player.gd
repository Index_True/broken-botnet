extends KinematicBody2D

export var speed = 400
export var velocity = Vector2()

func _physics_process(delta):
	velocity = Vector2(0,0)
	if Input.is_action_pressed("move_right"):
		velocity.x = speed
	if Input.is_action_pressed("move_left"):
		velocity.x = - speed
	if Input.is_action_pressed("move_down"):
		velocity.y = speed
	if Input.is_action_pressed("move_up"):
		velocity.y = - speed
		
	
	velocity = move_and_slide(velocity, Vector2(0, -1))
	
	look_at(get_global_mouse_position())
